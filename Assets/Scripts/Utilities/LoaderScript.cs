﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace VReddo.ShooterGame
{
    public class LoaderScript : MonoBehaviour
    {
        public string sceneName;
        public GameObject camObj;

        void Start()
        {
            //StartCoroutine(LoadAsyncScene());
            LoadScene(sceneName);
        }

        private void LoadScene(string sceneName)
        {
            if (sceneName.Contains("GameScene"))
            {
                camObj.gameObject.transform.position = new Vector3(0f, 0.9635369f, -5.690915f);
            }

            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }

        //IEnumerator LoadAsyncScene()
        //{
        //    AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

        //    // Wait until the asynchronous scene fully loads
        //    while (!asyncLoad.isDone)
        //    {
        //        yield return null;
        //    }
        //}
    }
}
