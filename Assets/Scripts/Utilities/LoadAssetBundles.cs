﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class LoadAssetBundles : MonoBehaviour
{
    public string prefabPath;
    //public string materialsPath;
    public string envPrefabName;
    public string bgmPrefabName;
    public GameObject loadingPanel;
    public GameObject HUDPanels;
    public GameObject highScorePanel;
    //public string skyBoxName;

    void Start()
    {
        StartCoroutine("GetPrefabAssetBundle");

        //StartCoroutine("GetMatAssetBundle");
    }

    IEnumerator GetPrefabAssetBundle()
    {
        //using (UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(prefabPath, 0, 1135620334))
        using (UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(prefabPath, 0, 255062651))
        {
            // wait for load to finish
            yield return www.SendWebRequest();
            if (www.error != null)
            {
                Debug.LogError("www error: " + www.error);
                yield break;
            }
            // get bundle from downloadhandler
            var bundle = ((DownloadHandlerAssetBundle)www.downloadHandler).assetBundle;
            var envPrefab = bundle.LoadAsset(envPrefabName);
            var bgmPrefab = bundle.LoadAsset(bgmPrefabName);

            HUDPanels.SetActive(true);
            highScorePanel.SetActive(true);
            loadingPanel.SetActive(false);
            Instantiate(envPrefab);
            Instantiate(bgmPrefab);
        }
    }
    //IEnumerator GetMatAssetBundle()
    //{
    //    using (UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(materialsPath, 0, 2058235538))
    //    {
    //        // wait for load to finish
    //        yield return www.SendWebRequest();
    //        if (www.error != null)
    //        {
    //            Debug.LogError("www error: " + www.error);
    //            yield break;
    //        }
    //        // get bundle from downloadhandler
    //        var bundle = ((DownloadHandlerAssetBundle)www.downloadHandler).assetBundle;
    //        var material = bundle.LoadAsset<Material>(skyBoxName);

    //        RenderSettings.skybox = material;
    //    }
    //}
}
