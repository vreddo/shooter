﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class TestScript : MonoBehaviour
{
    //private AssetBundleCreateRequest myLoadedAssetBundle;
    private AssetBundle myLoadedAssetBundle;
    public string path;
    public string prefabName;

    public string url;
    private AssetBundle sceneTest;

    GameObject waterCraftGO;

    void Start()
    {
        //OnBtnPressed();
        StartCoroutine("GetAssetBundle");
    }

    public void OnBtnPressed()
    {
        LoadAssetBundle(path);
        //myLoadedAssetBundle = AssetBundle.LoadFromFileAsync(Path.Combine(Application.streamingAssetsPath, "test"));
        //if (myLoadedAssetBundle == null)
        //{
        //    Debug.Log("Failed to load AssetBundle!");
        //    return;
        //}
        //var prefab = myLoadedAssetBundle.LoadAsset<GameObject>("MyObject");
        //Instantiate(prefab);

        //myLoadedAssetBundle.Unload(false);

        //sceneTest = AssetBundle.LoadFromFile(url);
        //Debug.Log(sceneTest == null ? " Failed to load AssetBundle" : " AssetBundle succesfully loaded");


        //string[] scenes = sceneTest.GetAllScenePaths();
        //string scene = Path.GetFileNameWithoutExtension(scenes[0]);
        //SceneManager.LoadScene(scene);
    }

    //IEnumerator Test()
    //{
    //    using (UnityWebRequest www = new UnityWebRequest(url))
    //    {
    //        yield return www;
    //        if (!string.IsNullOrEmpty(www.error))
    //        {
    //            Debug.LogError(www.error);
    //            yield break;
    //        }

    //        sceneTest = www.sceneTest;
    //    }
    //}

    IEnumerator GetAssetBundle()
    {
        using (UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(path, 0, 2667187529))
        {
            // wait for load to finish
            yield return www.SendWebRequest();
            if (www.error != null)
            {
                Debug.LogError("www error: " + www.error);
                yield break;
            }
            // get bundle from downloadhandler
           var bundle = ((DownloadHandlerAssetBundle)www.downloadHandler).assetBundle;
           var prefab = bundle.LoadAsset(prefabName);
           Instantiate(prefab);
        }

        //using (var uwr = new UnityWebRequest(path, UnityWebRequest.kHttpVerbGET))
        //{
        //    uwr.downloadHandler = new DownloadHandlerAssetBundle(path, 0);
        //    yield return uwr.SendWebRequest();
        //    AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);
        //    var prefab = bundle.LoadAsset(prefabName);
        //    Instantiate(prefab);
        //}

        //UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(path);
        //yield return www.SendWebRequest();

        //if (www.isNetworkError || www.isHttpError)
        //{
        //    Debug.Log(www.error);
        //}
        //else
        //{
        //    AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
        //    var prefab = bundle.LoadAsset(prefabName);
        //    Instantiate(prefab);
        //}
    }

    private void LoadAssetBundle(string bundleUrl)
    {
        myLoadedAssetBundle = AssetBundle.LoadFromFile(bundleUrl);

        Debug.Log(myLoadedAssetBundle == null ? " Failed to load AssetBundle" : " AssetBundle succesfully loaded");
        if (myLoadedAssetBundle != null)
            InstantiateObject(prefabName);
    }

    private void InstantiateObject(string assetName)
    {
        var prefab = myLoadedAssetBundle.LoadAsset(assetName);
        Instantiate(prefab);
    }

    private void Update()
    {
        //if (myLoadedAssetBundle == null)
        //    return;

        //if (myLoadedAssetBundle.isDone)
        //{
        //    {
        //        SceneManager.LoadScene("GameScene");
        //    }
        //}
    }


}
