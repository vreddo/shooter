﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Zenject;
using SimpleJSON;

namespace VReddo.ShooterGame
{
    public class HighScoreService : IInitializable, ILateDisposable
    {
        public enum Status
        {
            SUCCESS,
            TOKEN_EXPIRED,
            INVALID_DATA
        }
        public string highScoreJson = "";
        public string gameDataToSend = "";
        public string tokenStr = "";
        public Action OnFetchedData = null;

        private GameManager gameManager;
        private bool isPostingScore = false;

        public void Initialize()
        {
            //gameManager.OnEndGame += SendScoreToServer;
        }

        public void LateDispose()
        {
            //gameManager.OnEndGame -= SendScoreToServer;
        }

        [Inject]
        public void Constructor(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        // Get Token
        public IEnumerator GetToken(string uri)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Put(uri, SerializePlayerData()))
            {
                webRequest.method = "POST";
                webRequest.SetRequestHeader("Content-Type", "application/json");
                yield return webRequest.SendWebRequest();

                if (webRequest.isNetworkError)
                    Debug.Log(webRequest.error);
                else
                {
                    Debug.Log("TOKEN DATA!" + webRequest.downloadHandler.text);
                    var data = JSON.Parse(webRequest.downloadHandler.text);
                    tokenStr = data["payload"];
                    if (isPostingScore)
                        gameManager.UploadScore();
                    else
                        gameManager.UpdateScoreBoard();
                }
            }
        }

        // Get High Score Data
        public IEnumerator UpdateScoreBoard(string uri)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
            {
                // Request and wait for the desired page.
                webRequest.SetRequestHeader("Content-Type", "application/json");
                webRequest.SetRequestHeader("Authorization", "Bearer " + tokenStr);
                yield return webRequest.SendWebRequest();

                string data = "";

                if (webRequest.isNetworkError)
                    Debug.Log(webRequest.error);
                else
                {
                    Debug.Log("SCORE DATA!" + webRequest.downloadHandler.text);
                    data = webRequest.downloadHandler.text;
                    if (CheckStatus(data) == Status.SUCCESS)
                        DeserializeHighScore(data);
                    else if (CheckStatus(data) == Status.INVALID_DATA)
                        Debug.Log("INVALID DATA");
                }
            }
        }

        // Post High Score Data
        public IEnumerator UploadScore(string uri)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Put(uri, SerializeGameData()))
            {
                webRequest.method = "POST";
                webRequest.SetRequestHeader("Content-Type", "application/json");
                webRequest.SetRequestHeader("Authorization", "Bearer " + tokenStr);
                yield return webRequest.SendWebRequest();

                string data = "";

                if (webRequest.isNetworkError)
                    Debug.Log(webRequest.error);
                else
                {
                    Debug.Log("POST DATA!" + webRequest.downloadHandler.text);
                    data = webRequest.downloadHandler.text;

                    if (CheckStatus(data) == Status.TOKEN_EXPIRED)
                    {
                        isPostingScore = true;
                        gameManager.GetToken();
                    }
                    else if (CheckStatus(data) == Status.SUCCESS)
                    {
                        isPostingScore = false;
                        gameManager.UpdateScoreBoard();
                    }
                    else if(CheckStatus(data) == Status.INVALID_DATA)
                        Debug.Log("INVALID DATA");
                }
            }
        }

        // Serialize Player Data to get token
        // HARD CODED for the meantime
        private string SerializePlayerData()
        {
            string playerDataStr = "";
            PlayerData playerData = new PlayerData();
            playerData.uname = "admin";
            playerData.email = "henry.i@cfeduex.com";
            playerData.pword = "Redmako2018";
            playerDataStr = JsonUtility.ToJson(playerData);

            return playerDataStr;
        }

        // Serialize data
        private string SerializeGameData()
        {
            GameData gameData = new GameData();
            gameData.gameid = 1;
            switch (gameManager.curPlayerName)
            {
                case "Emer":
                    gameData.userid = 1;
                    break;
                case "Henry":
                    gameData.userid = 2;
                    break;
                case "JM":
                    gameData.userid = 3;
                    break;
                case "Pao":
                    gameData.userid = 4;
                    break;
                case "Zeus":
                    gameData.userid = 5;
                    break;
            }
            gameData.score = gameManager.computedScore;

            gameDataToSend = JsonUtility.ToJson(gameData);
            //Debug.Log("Serialized Data: " + gameDataToSend);

            return gameDataToSend;
        }

        // Force Setters for player 
        private void DeserializeHighScore(string jsonData)
        {
            highScoreJson = "";
            var data = JSON.Parse(jsonData);
            int payloadCount = data["payload"].Count;

            for (int i = 0; i < payloadCount; i++)
            {
                highScoreJson += i + 1 + "\t";
                switch (data["payload"][i]["userid"].Value)
                {
                    case "1":
                        highScoreJson += "Emer:  \t";
                        break;
                    case "2":
                        highScoreJson += "Henry: \t";
                        break;
                    case "3":
                        highScoreJson += "JM:    \t\t";
                        break;
                    case "4":
                        highScoreJson += "Pao:   \t";
                        break;
                    case "5":
                        highScoreJson += "Zeus:  \t";
                        break;
                }
                highScoreJson += data["payload"][i]["score"] + "\n";
            }
            //Debug.Log("Deserialized Data: " + highScoreJson);
            if (OnFetchedData != null)
                OnFetchedData();
        }

        private Status CheckStatus(string msg)
        {
            var data = JSON.Parse(msg);
            string statusMessage = data["status_message"].Value;

            if (statusMessage.Contains("expired") || statusMessage.Contains("Expired"))
                return Status.TOKEN_EXPIRED;
            else if (statusMessage.Contains("invalid") || statusMessage.Contains("Invalid"))
                return Status.INVALID_DATA;
            else
                return Status.SUCCESS;
        }
    }

    [System.Serializable]
    public class GameData
    {
        public int gameid;
        public int userid;
        public float score;
    }

    [System.Serializable]
    public class PlayerData
    {
        public string uname;
        public string email;
        public string pword;
    }
}
