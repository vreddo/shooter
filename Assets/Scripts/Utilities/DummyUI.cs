﻿using UnityEngine;
using TMPro;
using Zenject;

namespace VReddo.ShooterGame
{
    public class DummyUI : MonoBehaviour
    {
        public TextMeshProUGUI playerNameText;

        private GameManager gameManager;
        private string strPlayerName = "Emer";

        void Start()
        {
            SetPlayerName(strPlayerName);
        }

        [Inject]
        public void Constructor(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        public void SetPlayerName(string _playername)
        {
            strPlayerName = _playername;
            gameManager.curPlayerName = strPlayerName;
            playerNameText.text = strPlayerName;
        }
    }
}
