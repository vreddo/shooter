﻿using UnityEngine;

namespace VReddo.ShooterGame
{
    public enum SFXClips
    {
        SCORE,
        GUN_FIRE,
        METAL_HIT,
        BUTTON_CLICK,
        GUN_EMPTY,
        RELOAD
    }

    public class SFXManager : MonoBehaviour
    {
        public AudioClip[] sfxClip;
        [SerializeField] private AudioSource[] audioSource;

        #region Public Methods
        public void PlayClip(SFXClips clip)
        {
            switch (clip)
            {
                case SFXClips.SCORE:
                    audioSource[0].Play();
                    break;
                case SFXClips.GUN_FIRE:
                    audioSource[0].clip = sfxClip[0];
                    audioSource[0].Play();
                    break;
                case SFXClips.METAL_HIT:
                    audioSource[1].clip = sfxClip[1];
                    audioSource[1].Play();
                    break;
                case SFXClips.BUTTON_CLICK:
                    audioSource[0].clip = sfxClip[2];
                    audioSource[0].Play();
                    break;
                case SFXClips.GUN_EMPTY:
                    audioSource[0].clip = sfxClip[3];
                    audioSource[0].Play();
                    break;
                case SFXClips.RELOAD:
                    audioSource[0].clip = sfxClip[4 ];
                    audioSource[0].Play();
                    break;
            }
        }

        public void StopClip(SFXClips clip)
        {
            switch (clip)
            {
                case SFXClips.SCORE:
                    audioSource[0].Stop();
                    break;
                case SFXClips.GUN_FIRE:
                    audioSource[0].Stop();
                    break;
                case SFXClips.METAL_HIT:
                    audioSource[1].Stop();
                    break;
                case SFXClips.BUTTON_CLICK:
                    audioSource[0].Stop();
                    break;
            }
        }
        #endregion
    }
}
