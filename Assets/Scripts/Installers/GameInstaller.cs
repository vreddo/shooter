using UnityEngine;
using Zenject;

namespace VReddo.ShooterGame
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private GameManager _gameManager;
        [SerializeField] private SFXManager _sfxManager;

        public GameObject bulletGO;
        public GameObject floatingText;

        public override void InstallBindings()
        {
            Container.Bind<GameManager>().FromInstance(_gameManager);
            Container.Bind<SFXManager>().FromInstance(_sfxManager);
            Container.BindInterfacesAndSelfTo<PlayerEvents>().AsSingle();
            Container.BindInterfacesAndSelfTo<HighScoreService>().AsSingle();

            Container.BindFactory<float, float, BulletScript, BulletScript.Factory>()
                .FromPoolableMemoryPool<float, float, BulletScript, BulletPool>
                (poolBinder => poolBinder.WithInitialSize(15).FromComponentInNewPrefab(bulletGO).
                UnderTransformGroup("Bullets"));

            Container.BindFactory<float, FloatingText, FloatingText.Factory>()
               .FromPoolableMemoryPool<float, FloatingText, FloatingTextPool>
               (poolBinder => poolBinder.WithInitialSize(10).FromComponentInNewPrefab(floatingText).
               UnderTransformGroup("FloatingTexts"));
        }
    }

    class BulletPool : MonoPoolableMemoryPool<float, float, IMemoryPool, BulletScript>
    {
    }

    class FloatingTextPool : MonoPoolableMemoryPool<float, IMemoryPool, FloatingText>
    {
    }
}