﻿using UnityEngine;

namespace VReddo.ShooterGame
{
    public class MuzzleEffect : MonoBehaviour
    {
        public ParticleSystem[] particleSystem;

        public void PlayParticles()
        {
            for(int i = 0; i < particleSystem.Length; i++)
            {
                particleSystem[i].Play();
            }
        }
    }
}
