﻿using TMPro;
using UnityEngine;
using Zenject;

namespace VReddo.ShooterGame
{
    public class ResultScreen : MonoBehaviour
    {
        #region Private Variables
        [SerializeField] private TextMeshProUGUI scoreTxt;
        [SerializeField] private TextMeshProUGUI bodyTxt;
        [SerializeField] private TextMeshProUGUI computeTxt;
        private GameManager gameManager;
        #endregion

        #region Public Methods
        [Inject]
        public void Constructor(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        public void DisplayResult(float _score, float fired, float hit)
        {
            float accuracy = 0f;
            float finalScore = 0;
            accuracy = (hit / fired) * 100f;
            finalScore = _score * accuracy;
            gameManager.computedScore = Mathf.Round(finalScore);

            bodyTxt.text = "Fired: " + fired + "\nHit: " + hit + "\nAccuracy: " + 
                Mathf.Round(accuracy) + "%" + "\nScore: " + _score;
            computeTxt.text = "Score x Accuracy" + "\n" + _score + " x " + Mathf.Round(accuracy);
            scoreTxt.text = "Final Score: " + Mathf.Round(finalScore);
        }
        #endregion
    }
}
