﻿using UnityEngine;
using TMPro;
using Zenject;

namespace VReddo.ShooterGame
{
    public class HighscoreDisplay : MonoBehaviour
    {
        public TextMeshProUGUI highScoreText;

        private GameManager gameManager;
        private HighScoreService highScoreService;
        private string URL = "http://192.168.1.181:8080/v1/topten/2";

        #region Monobehaviour Methods
        void OnEnable()
        {
            highScoreService.OnFetchedData += OnFetchedData;
        }

        void OnDisable()
        {
            highScoreService.OnFetchedData -= OnFetchedData;
        }

        void Start()
        {
            gameManager.GetToken();
        }
        #endregion

        [Inject]
        public void Constructor(GameManager _gameManager, HighScoreService _highScoreService)
        {
            gameManager = _gameManager;
            highScoreService = _highScoreService;
        }

        private void OnFetchedData()
        {
            highScoreText.text = highScoreService.highScoreJson;
        }
    }

    
}
