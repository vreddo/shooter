﻿using UnityEngine;
using TMPro;
using Zenject;

namespace VReddo.ShooterGame
{
    [RequireComponent(typeof(BulletSpawner))]
    [RequireComponent(typeof(TimerScript))]
    public class GameManager : MonoBehaviour
    {
        #region Public Variables
        public TargetController[] targets;
        public float gameTime;
        public int civilianChancePercentage;
        public int bulletCount;
        public string curPlayerName = "";

        // Used by player events
        public GameObject m_LeftAnchor;
        public GameObject m_RightAnchor;
        public GameObject m_HeadAnchor;

        [HideInInspector] public float bulletsHit = 0f;
        [HideInInspector] public float bulletsFire = 0f;
        [HideInInspector] public float computedScore;
        [HideInInspector] public float timeRemaining = 0;
        [HideInInspector] public bool isFirstRun;
        [HideInInspector] public bool gameStarted = false;
        [HideInInspector] public int score;
        #endregion

        #region Private Variables
        private FloatingText.Factory floatingTxtFactory;
        private BulletSpawner bulletSpawner;
        private TimerScript timer;
        private PlayerEvents playerEvents;
        private HighScoreService highScoreService;
        private SFXManager sfxManager;
        [SerializeField] private GameObject startPanel;
        [SerializeField] private GameObject reloadObj;
        [SerializeField] private ResultScreen resultPanel;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI timerText;
        [SerializeField] private TextMeshProUGUI bulletCntTxt;
        [SerializeField] private TextMeshProUGUI startMsgText;
        [SerializeField] private TextMeshProUGUI fpsText;

        private float deltaTime = 0.0f; //FPS

        int m_frameCounter = 0;
        float m_timeCounter = 0.0f;
        float m_lastFramerate = 0.0f;
        public float m_refreshTime = 0.5f;
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            bulletSpawner = GetComponent<BulletSpawner>();
            timer = GetComponent<TimerScript>();
            playerEvents.OnControllerSource += UpdateOrigin;
            playerEvents.OnTouchPadDown += OnTouchPadDown;
        }

        void Update()
        {
            //deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;

            //float msec = deltaTime * 1000.0f;
            //float fps = 1.0f / deltaTime;
            //string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            if (m_timeCounter < m_refreshTime)
            {
                m_timeCounter += Time.deltaTime;
                m_frameCounter++;
            }
            else
            {
                //This code will break if you set your m_refreshTime to 0, which makes no sense.
                m_lastFramerate = (float)m_frameCounter / m_timeCounter;
                m_frameCounter = 0;
                m_timeCounter = 0.0f;
            }

            fpsText.text = m_lastFramerate.ToString() + " fps";

            if (Input.GetKeyDown(KeyCode.R))
            {
                bulletSpawner.BulletCount = 20;
                UpdateBulletCount(bulletSpawner.BulletCount);
            }
        }

        void OnDestroy()
        {
            playerEvents.OnControllerSource -= UpdateOrigin;
            playerEvents.OnTouchPadDown -= OnTouchPadDown;
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Construct(PlayerEvents _playerEvents, 
            FloatingText.Factory _floatingTxtFactory, 
            HighScoreService _highScoreService,
            SFXManager _sfxManager)
        {
            playerEvents = _playerEvents;
            floatingTxtFactory = _floatingTxtFactory;
            highScoreService = _highScoreService;
            sfxManager = _sfxManager;
        }

        //START GAME
        public void StartGame()
        {
            Debug.Log("----------------");
            ResetTextVal();
            isFirstRun = true;
            gameStarted = true;
            timer.StartCountingDown();
            IsCursorVisible(false);
            startPanel.SetActive(false);
            reloadObj.SetActive(false);
            resultPanel.gameObject.SetActive(false);
            sfxManager.PlayClip(SFXClips.BUTTON_CLICK);
        }

        //Add score UI
        public void AddScore(int _score)
        {
            score += _score;
            scoreText.text = "Score: " + score;
            ShowFloatingText(false, _score);
        }

        //Deduct score UI
        public void DeductScore(int _score)
        {
            score -= _score;
            if (score <= 0)
            {
                score = 0;
            }

            scoreText.text = "Score: " + score;
            ShowFloatingText(true, 2);
        }

        //Updates Bullet Count
        public void UpdateBulletCount(int bulletCnt)
        {
            bulletCntTxt.text = "Bullets: " + bulletCnt.ToString() + " / 10"; ;

            if (bulletCnt == 0)
                reloadObj.SetActive(true);
            else
            {
                sfxManager.PlayClip(SFXClips.RELOAD);
                reloadObj.SetActive(false);
            }
        }

        //Update game timer
        public void UpdateTime(float _timeRemaining)
        {
            timerText.text = "Time: " + _timeRemaining.ToString();
            timeRemaining = _timeRemaining;

            //END GAME
            if (_timeRemaining <= 0)
            {
                reloadObj.SetActive(false);
                resultPanel.DisplayResult(score, bulletsFire, bulletsHit);
                resultPanel.gameObject.SetActive(true);
                gameStarted = false;
                IsCursorVisible(true);

                foreach (TargetController _targets in targets)
                {
                    _targets.DestroySpwnObj();
                }

                UploadScore();
            }
        }
        
        // Get Token
        public void GetToken()
        {
            StartCoroutine(highScoreService.GetToken("http://192.168.1.253:8080/api/gettoken/"));
        }

        // Updates high ScoreBoard
        public void UpdateScoreBoard()
        {
            StartCoroutine(highScoreService.UpdateScoreBoard("http://192.168.1.253:8080/api/topten/1"));
        }

        // Upload scores to server
        public void UploadScore()
        {
            StartCoroutine(highScoreService.UploadScore("http://192.168.1.253:8080/api/highscores/"));
        }
        #endregion

        #region Private Methods
        //Show Floating Text feedback UI
        private void ShowFloatingText(bool isDeduct, int points)
        {
            Vector3 scoreTextPos = new Vector3(scoreText.gameObject.transform.position.x + 0.8f, 
                scoreText.gameObject.transform.position.y + 0.01f, 
                scoreText.gameObject.transform.position.z);

            //var go = Instantiate(floatingTextPrefab, transform.position, Quaternion.identity, transform);
            var go = floatingTxtFactory.Create(0.5f);
            go.transform.position = scoreTextPos;
            go.transform.rotation = Quaternion.identity;
            var textMesh = go.GetComponent<TextMeshPro>();
            if (isDeduct)
            {
                textMesh.color = new Color32(0, 255, 0, 255);
                textMesh.text = "-" + points;
            }
            else
            {
                textMesh.color = new Color32(255, 0, 0, 255);
                textMesh.text = "+" + points;
            }
        }

        //Reset texts values and variables
        private void ResetTextVal()
        {
            score = 0;
            bulletSpawner.BulletCount = bulletCount;
            //timer.TimeRemaining = gameTime;

            scoreText.text = "Score: " + score;
            bulletCntTxt.text = "Bullets: " + bulletCount + " / 10";
            timerText.text = "Time: " + timer.TimeRemaining;
        }

        //Set cursor visibility
        private void IsCursorVisible(bool _cursorVisible)
        {
            Cursor.lockState = (_cursorVisible) ? CursorLockMode.None : CursorLockMode.Locked;
            Cursor.visible = _cursorVisible;
        }

        //Reload Gun
        private void OnTouchPadDown()
        {
            bulletSpawner.BulletCount = this.bulletCount;
            UpdateBulletCount(bulletSpawner.BulletCount);
        }

        //Set the active gun object based on controller settings
        private void UpdateOrigin(OVRInput.Controller controller, GameObject controllerObject)
        {
           
        }
        #endregion
    }
}
