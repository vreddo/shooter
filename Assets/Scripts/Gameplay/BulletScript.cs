﻿using UnityEngine;
using Zenject;

namespace VReddo.ShooterGame
{
    public class BulletScript : MonoBehaviour, IPoolable<float, float, IMemoryPool>
    {
        #region Private Variables
        [SerializeField] private float speed;
        [SerializeField] private ParticleSystem[] hitParticles;
        [SerializeField] private GameObject projectile;
        private Vector3 mPrevPos;
        private float time;
        private IMemoryPool pool;
        private float startTime;
        private float lifeTime;

        private Rigidbody rb;
        private SFXManager sfxManager;
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        void Start()
        {
            time = 0;
            mPrevPos = transform.position;
        }

        void Update()
        {
            mPrevPos = transform.position;
            transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);

            RaycastHit[] hits = Physics.RaycastAll(new Ray(mPrevPos, (transform.position - mPrevPos).normalized), (transform.position - mPrevPos).magnitude);

            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].transform.gameObject.tag == "Target")
                {
                    speed = 0;
                    hits[i].transform.gameObject.GetComponent<TargetScript>().OnHit();
                    
                }

                if (hits[i].transform.gameObject.tag != "Bullet")
                {
                    if (pool == null)
                        return;

                    projectile.SetActive(false);

                    for (int j = 0; j < hitParticles.Length; j++)
                    {
                        hitParticles[j].Play();
                    }
                    //sfxManager.PlayClip(SFXClips.METAL_HIT);
                    //pool.Despawn(this);
                }
            }

            if (Time.realtimeSinceStartup - startTime > lifeTime)
            {
                pool.Despawn(this);
            }
        }
        #endregion

        #region Zenject Methods
        [Inject]
        public void Constructor(SFXManager _sfxManager)
        {
            sfxManager = _sfxManager;
        }

        public void OnDespawned()
        {
            transform.rotation = Quaternion.identity;
            pool = null;
        }

        public void OnSpawned(float _speed, float _lifeTime, IMemoryPool _pool)
        {
            projectile.SetActive(true);

            pool = _pool;
            speed = _speed;
            lifeTime = _lifeTime;

            startTime = Time.realtimeSinceStartup;
        }
        #endregion

        public class Factory : PlaceholderFactory<float, float, BulletScript>
        {
        }
    }
}
