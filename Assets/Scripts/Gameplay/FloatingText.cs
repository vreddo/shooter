﻿using UnityEngine;
using Zenject;

namespace VReddo.ShooterGame
{
    public class FloatingText : MonoBehaviour, IPoolable<float, IMemoryPool>
    {
        #region Public Variables
        //public Vector3 offset = new Vector3(0, 0.1f, 0);
        #endregion

        #region Private Variables
        private float startTime;
        private float lifeTime;
        private Vector3 initPos;
        private IMemoryPool pool;
        #endregion

        #region Private Methods
        void Update()
        {
            if (Time.realtimeSinceStartup - startTime > lifeTime)
            {
                pool.Despawn(this);
            }
        }
        #endregion

        #region Zenject Methods
        public void OnDespawned()
        {
            pool = null;
        }

        public void OnSpawned(float _lifeTime, IMemoryPool _pool)
        {
            pool = _pool;
            lifeTime = _lifeTime;

            //transform.localPosition += offset;
            //initPos = new Vector3(transform.position.x, 1.3f, transform.position.z);
            //transform.position = initPos;

            startTime = Time.realtimeSinceStartup;
        }
        #endregion

        public class Factory : PlaceholderFactory<float, FloatingText>
        {
        }
    }
}
