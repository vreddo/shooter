﻿using UnityEngine;

namespace VReddo.ShooterGame
{
    public class PlayerShooter : MonoBehaviour
    {
        #region Private Variables
        private Camera playerCam;
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            playerCam = GetComponent<Camera>();
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 point = new Vector3(playerCam.pixelWidth / 2, playerCam.pixelHeight / 2, 0);
                Ray ray = playerCam.ScreenPointToRay(point);

                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    GameObject hitObject = hit.transform.gameObject;
                }
            }
        }

        void OnGUI()
        {
            int size = 12;
            float posX = playerCam.pixelWidth / 2 - size / 4;
            float posY = playerCam.pixelHeight / 2 - size / 2;
            GUI.Label(new Rect(posX, posY, size, size), "*");
        }
        #endregion
    }
}
