﻿using UnityEngine;
using UnityEngine.UI;

namespace VReddo.ShooterGame
{
    public class TimerScript : MonoBehaviour
    {
        #region Public Variables
        public float TimeRemaining
        {
            get
            {
                return timeRemaining;
            }
            set
            {
                timeRemaining = value;
            }
        }
        #endregion

        #region Private Variables
        private GameManager gameManager;
        private float timeRemaining;
        private bool isCountingDown = false;
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            gameManager = GetComponent<GameManager>();
        }
        #endregion

        #region Public Methods
        public void StartCountingDown()
        {
            if (!isCountingDown)
            { 
                isCountingDown = true;
                timeRemaining = gameManager.gameTime;

                gameManager.UpdateTime(timeRemaining);
                Invoke("_Tick", 1f);
            }
        }
        #endregion

        #region Private Methods
        private void _Tick()
        {
            timeRemaining--;

            if (timeRemaining > 0)
                Invoke("_Tick", 1f);
            else
                isCountingDown = false;

            gameManager.UpdateTime(timeRemaining);
        }
        #endregion
    }
}
