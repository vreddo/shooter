﻿using UnityEngine;
using Zenject;

namespace VReddo.ShooterGame
{
    [RequireComponent(typeof(GameManager))]
    public class BulletSpawner : MonoBehaviour
    {
        #region Private Variables
        [SerializeField] private GameObject bulletSpawnPoint;
        //[SerializeField] private GameObject bulletPrefab;

        private GameManager gameManager;
        private PlayerEvents playerEvents;
        private SFXManager sfxManager;
        private bool isShooting = false;
        private float timeToFire = 0;
        private BulletScript.Factory bulletFactory;
        private int _bulletCount;
        private OVRInput.Controller activeController;
        #endregion

        #region Public Variables
        public GameObject gunRight;
        public GameObject gunLeft;
        public float _fireRate = 10;

        public int BulletCount
        {
            get
            {
                return _bulletCount;
            }
            set
            {
                _bulletCount = value;
            }
        }
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            gameManager = GetComponent<GameManager>();
            playerEvents.OnControllerSource += UpdateOrigin;
            playerEvents.OnIndexTriggerDown += OnIndexTriggerDown;
        }

        void Start()
        {
            this.BulletCount = gameManager.bulletCount;
        }

        void Update()
        {
            if (this.BulletCount <= 0 || !gameManager.gameStarted)
                return;

            if (Application.isEditor)
            {
                if (Input.GetMouseButtonDown(0) && Time.time >= timeToFire)
                {
                    timeToFire = Time.time + 1 / _fireRate;
                    SpawnBullet();
                }
            }
                
            if (isShooting && Time.time >= timeToFire)
            {
                timeToFire = Time.time + 1 / _fireRate;
                SpawnBullet();
                isShooting = false;
            }
        }

        void OnDestroy()
        {
            playerEvents.OnControllerSource -= UpdateOrigin;
            playerEvents.OnIndexTriggerDown -= OnIndexTriggerDown;
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Construct(PlayerEvents _playerEvents, 
            BulletScript.Factory _bulletFactory, 
            SFXManager _sfxManager)
        {
            playerEvents = _playerEvents;
            bulletFactory = _bulletFactory;
            sfxManager = _sfxManager;
        }
        #endregion

        #region Private Methods
        //Spawns Bullets
        private void SpawnBullet()
        {
            if (bulletSpawnPoint)
            {
                gameManager.bulletsFire++;
                this.BulletCount--;
                gameManager.UpdateBulletCount(this.BulletCount);
                var bullet = bulletFactory.Create(50.0f, 1.5f);
                bullet.transform.position = bulletSpawnPoint.transform.position + bulletSpawnPoint.transform.forward * 0.5f;
                bullet.transform.rotation = bulletSpawnPoint.transform.rotation;

                sfxManager.PlayClip(SFXClips.GUN_FIRE);
                if (activeController == OVRInput.Controller.RTrackedRemote)
                    gunRight.GetComponent<MuzzleEffect>().PlayParticles();
                else if (activeController == OVRInput.Controller.LTrackedRemote)
                    gunLeft.GetComponent<MuzzleEffect>().PlayParticles();
            }
        }

        //Called if player is pressing fire on the controller
        private void OnIndexTriggerDown()
        {
            if (this.BulletCount > 0 && gameManager.gameStarted)
                isShooting = true;
            else if (this.BulletCount <= 0 && gameManager.gameStarted)
                sfxManager.PlayClip(SFXClips.GUN_EMPTY);
        }

        //Sets bullet spawnpoint base on controller settings
        private void UpdateOrigin(OVRInput.Controller controller, GameObject controllerObject)
        {
            bulletSpawnPoint = controllerObject;
            activeController = controller;

            Debug.Log("---------UpdateOrigin  activeController: " + activeController + " | " + "bulletSpawnPoint: " + bulletSpawnPoint);
            if (controller == OVRInput.Controller.RTouch)
            {
                gunRight.SetActive(true);
                gunLeft.SetActive(false);
            }

            if (controller == OVRInput.Controller.RTrackedRemote)
            {
                gunRight.SetActive(true);
                gunLeft.SetActive(false);
            }

            if (controller == OVRInput.Controller.LTrackedRemote)
            {
                gunRight.SetActive(false);
                gunLeft.SetActive(true);
            }
        }
        #endregion
    }
}
