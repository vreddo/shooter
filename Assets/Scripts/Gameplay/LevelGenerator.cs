﻿using System.Collections.Generic;
using UnityEngine;

namespace VReddo.ShooterGame
{
    public class LevelGenerator : MonoBehaviour
    {
        #region Private Variables
        private GameManager gameManager;
        private TimerScript timer;
        private List<int> uniqueNumbers;
        private List<int> finishedList;
        private int targetsCount;
        private float nextActionTime = 0.0f;
        [SerializeField] private float interval = 3.0f;
        private int enemyCountToSpawn = 0;
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            gameManager = GetComponent<GameManager>();
            timer = GetComponent<TimerScript>();
        }

        void Start()
        {
            uniqueNumbers = new List<int>();
            finishedList = new List<int>();
            targetsCount = gameManager.targets.Length;
        }

        void Update()
        {
            if (!gameManager.gameStarted || timer.TimeRemaining < 0)
                return;

            if (timer.TimeRemaining <= 60 && timer.TimeRemaining > 40)
            {
                interval = 3;
                enemyCountToSpawn = 4;
            } 
            else if (timer.TimeRemaining <= 40 && timer.TimeRemaining > 20)
            {
                interval = 2;
                enemyCountToSpawn = 5;
            } 
            else if (timer.TimeRemaining <= 20 && timer.TimeRemaining >= 0)
            {
                interval = 2;
                enemyCountToSpawn = 6;
            }

            if (gameManager.isFirstRun)
            {
                nextActionTime = 0;
                RandomTarget(enemyCountToSpawn);
                gameManager.isFirstRun = false;
            }

            if (nextActionTime > interval)
            {
                nextActionTime = 0;
                RandomTarget(enemyCountToSpawn);
            }

            nextActionTime += Time.deltaTime;
        }
        #endregion

        #region Private Methods
        // Selects a random target
        private void RandomTarget(int spawnCount)
        {
            uniqueNumbers = new List<int>();
            finishedList = new List<int>();

            for (int i = 0; i <= targetsCount; i++)
            {
                uniqueNumbers.Add(i);
            }
            for (int i = 0; i < spawnCount; i++)
            {
                int randNum = uniqueNumbers[Random.Range(0, uniqueNumbers.Count - 1)];
                finishedList.Add(randNum);
                uniqueNumbers.Remove(randNum);
            }

            foreach (int _index in finishedList)
            {
                gameManager.targets[_index].StartGeneratingTargets();
            }
        }
        #endregion
    }
}
