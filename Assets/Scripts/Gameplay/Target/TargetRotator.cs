﻿using System.Collections;
using UnityEngine;
using Zenject;

namespace VReddo.ShooterGame
{
    public class TargetRotator : MonoBehaviour
    {
        #region Public Variables
        public enum TargetType
        {
            FLIPRIGHT,
            FLIPLEFT,
            FLIPBOTTOM,
            FLIPTOP,
        }

        public TargetType targetType;
        #endregion

        #region Private Variables
        [SerializeField] private float smooth = 10f;
        private TargetController targetController;
        private TargetMover targetMover;
        private Quaternion target;

        private Quaternion originalRot;
        private Vector3 originalPos;

        private GameManager gameManager;    
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            targetController = GetComponent<TargetController>();
            targetMover = GetComponent<TargetMover>();
        }

        void Start()
        {
            originalPos = transform.position;
            originalRot = transform.rotation;
        }

        void Update()
        {
            if (!gameManager.gameStarted)
                return;

            if (targetController.spawnedTarget == null)
                return;

            if (targetController.isShowTarget)
                Lerp(0, 0);
            else
                RetrackFlip();

        }
        #endregion

        #region Public Methods
        [Inject]
        public void Construct(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        //Resets parent rotation ans position;
        public void ResetRotPos()
        {
            this.transform.SetPositionAndRotation(originalPos, originalRot);
        }
        #endregion

        #region Private Methods
        //Start lerping to desired rotation
        private void Lerp(float lerpToX, float lerpToY)
        {
            target = Quaternion.Euler(lerpToX, lerpToY, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);

            if (targetMover.canMove)
                targetMover.StartMoving();
        }

        //Retracks animation flip
        private void RetrackFlip()
        {
            switch (targetType)
            {
                case TargetType.FLIPTOP:
                    Lerp(90, 0);
                    if (transform.localEulerAngles.x > 89.5f)
                        targetController.DestroySpwnObj();
                    break;
                case TargetType.FLIPRIGHT:
                    Lerp(0, -90);
                    if (transform.localEulerAngles.y < 271.5)
                        targetController.DestroySpwnObj();
                    break;
                case TargetType.FLIPLEFT:
                    Lerp(0, 90);
                    if (transform.localEulerAngles.y > 89.5)
                        targetController.DestroySpwnObj();
                    break;
                case TargetType.FLIPBOTTOM:
                    Lerp(-90, 0);
                    if (transform.localEulerAngles.x < 270.5f)
                        targetController.DestroySpwnObj();
                    break;
            }

            targetMover.SetIsMoving = false;
        }
        #endregion
    }
}
