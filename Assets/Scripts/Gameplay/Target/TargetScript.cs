﻿using UnityEngine;
using UnityEngine.Events;

namespace VReddo.ShooterGame
{
    public class TargetScript : MonoBehaviour
    {
        #region Public Variables
        //public GameObject floatingTextPrefab;
        [HideInInspector] public UnityEvent OnTargetHit;
        #endregion

        #region Public Methods
        public void OnHit()
        {
            OnTargetHit.Invoke();
        }
        #endregion
    }
}
