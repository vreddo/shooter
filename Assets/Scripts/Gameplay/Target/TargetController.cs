﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace VReddo.ShooterGame
{
    //Handles spawning, despawning and target detection
    public class TargetController : MonoBehaviour
    {
        #region Public Variables
        public enum TargetTypeEnum
        {
            Empty,
            Civilian,
            Target
        }
        [HideInInspector] public TargetTypeEnum targetType;

        //public GameObject floatingtextprefab;
        public GameObject spawnPoint;
        [HideInInspector] public float retractAnimDelay;
        public int givenPoints;
       
        [HideInInspector] public bool isShowTarget;
        [HideInInspector] public GameObject spawnedTarget;
        [HideInInspector] public TargetRotator targetRotator;
        [HideInInspector] public TargetMover targetMover;
        #endregion

        #region Private Variables
        private GameManager gameManager;
        private Coroutine targetCoroutine;
        private bool isAvailable;
        private List<int> percentage = new List<int>();
        [SerializeField] private GameObject[] targetsPrefab;
        #endregion

        void Awake()
        {
            targetRotator = GetComponent<TargetRotator>();
            targetMover = GetComponent<TargetMover>();
        }

        #region Monobehaviour Methods
        void Start()
        {
            isShowTarget = false;
            isAvailable = true;

            for (int i = 0; i <= 100; i++)
            {
                percentage.Add(i);
            }
        }
        #endregion

        #region Private Methods
        //Instantiate target
        private void SpawnTarget()
        {
            spawnedTarget = Instantiate(PickPrefab(), spawnPoint.transform.position, spawnPoint.transform.rotation);
            spawnedTarget.transform.parent = spawnPoint.transform;
            spawnedTarget.GetComponent<TargetScript>().OnTargetHit.AddListener(OnTargetHit);
        }

        //Set the target type
        private void SetType(TargetTypeEnum _type)
        {
            targetType = _type;
        }

        //Selects prefab to load
        private GameObject PickPrefab()
        {
            int randNum = Random.Range(0, percentage.Count);
            if (randNum < gameManager.civilianChancePercentage)
            {
                targetType = TargetTypeEnum.Civilian;
                return targetsPrefab[0];
            }
            else
            {
                targetType = TargetTypeEnum.Target;
                return targetsPrefab[1];
            }
        }

        //Plays flip animation with delay
        private IEnumerator ShowTarget(bool show, float delay)
        {
            yield return new WaitForSeconds(delay);
            isShowTarget = false;
        }

        //Listens if something hit the spawned target prefab
        private void OnTargetHit()
        {
            if (targetType == TargetTypeEnum.Empty)
                return;

            isShowTarget = false;
            spawnedTarget.GetComponent<MeshCollider>().enabled = false;
            StopCoroutine(targetCoroutine);

            if (targetType == TargetTypeEnum.Target)
            {
                gameManager.AddScore(givenPoints);
                gameManager.bulletsHit++;
            }
            else if (targetType == TargetTypeEnum.Civilian)
                gameManager.DeductScore(2);
        }

        //Controls the duration of target being shown base on time left
        private float RetractTime()
        {
            if (gameManager.timeRemaining <= 60 && gameManager.timeRemaining > 40)
                retractAnimDelay = 3.5f;
            else if (gameManager.timeRemaining <= 40 && gameManager.timeRemaining > 20)
            {
                retractAnimDelay = Random.Range(2.5f, 3.5f);
            }
            else if (gameManager.timeRemaining <= 20 && gameManager.timeRemaining >= 0)
            {
                retractAnimDelay = Random.Range(1.5f, 2.5f);
            }

            return retractAnimDelay;
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Construct(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        //Initially called to generate targets
        public void StartGeneratingTargets()
        {
            if (!isAvailable)
                return;

            isAvailable = false;
            SpawnTarget();
            isShowTarget = true;
            targetCoroutine = StartCoroutine(ShowTarget( false, RetractTime() ));
        }

        //Destroy spawned target prefab
        public void DestroySpwnObj()
        {
            if (!spawnedTarget)
                return;

            targetRotator.ResetRotPos();
            isAvailable = true;
            spawnedTarget.GetComponent<TargetScript>().OnTargetHit.RemoveListener(OnTargetHit);
            SetType(TargetTypeEnum.Empty);
            Destroy(spawnedTarget);
        }  
        #endregion
    }
}
