﻿using UnityEngine;
using Zenject;

namespace VReddo.ShooterGame
{
    public class TargetMover : MonoBehaviour
    {
        #region Public Variables
        public enum MoveDirection
        {
            MOVERIGHT,
            MOVELEFT
        }

        public MoveDirection moveDirection;
        public float speed = 0.5f;
        public bool canMove;
        public bool SetIsMoving
        {
            set
            {
                isMoving = value;
            }
        }
        #endregion

        #region Private Variables
        private bool isMoving = false;
        private GameManager gameManager;
        #endregion

        #region Monobehaviour Methods
        void Update()
        {
            if (!canMove || !gameManager.gameStarted)
                return;

            if (isMoving)
            {
                switch (moveDirection)
                {
                    case MoveDirection.MOVELEFT:
                        transform.Translate(Vector3.left * (Time.deltaTime * speed));
                        break;
                    case MoveDirection.MOVERIGHT:
                        transform.Translate(Vector3.right * (Time.deltaTime * speed));
                        break;
                }
            }    
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Construct(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        //Start the target movement;
        public void StartMoving()
        {
            isMoving = true;
        }
        #endregion
    }
}
