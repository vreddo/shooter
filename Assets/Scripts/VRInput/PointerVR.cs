﻿using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace VReddo.ShooterGame
{
    public class PointerVR : MonoBehaviour
    {
        [HideInInspector] public float m_Distance = 10.0f;
        public LineRenderer m_LineRenderer = null;
        public LayerMask m_EverythingMask = 0;
        public LayerMask m_InteractableMask = 0;
        public UnityAction<Vector3, GameObject> OnPointerUpdate = null;

        private Transform m_CurrentOrigin = null;
        private GameObject m_CurrentObject = null;
        private GameManager gameManager;
        private PlayerEvents playerEvents;

        void Awake()
        {
            m_LineRenderer.enabled = false; // Force Value: For Shooter Game

            playerEvents.OnControllerSource += UpdateOrigin;
            playerEvents.OnTouchPadDown += ProcessTouchPadDown;
        }

        void Start()
        {
            SetLineColor();
        }

        void Update()
        {
            if (m_CurrentOrigin == null)
                return;

            if (!gameManager.gameStarted)
                m_Distance = 5;
            else
                m_Distance = 3;

            Vector3 hitPoint = UpdateLine();

            m_CurrentObject = UpdatePointerStatus();

            if (OnPointerUpdate != null)
                OnPointerUpdate(hitPoint, m_CurrentObject);
        }

        void OnDestroy()
        {
            playerEvents.OnControllerSource -= UpdateOrigin;
            playerEvents.OnTouchPadDown -= ProcessTouchPadDown;
        }

        [Inject]
        public void Construct(GameManager _gameManager, PlayerEvents _playerEvents)
        {
            gameManager = _gameManager;
            playerEvents = _playerEvents;
        }

        private Vector3 UpdateLine()
        {
            // Create Ray
            RaycastHit hit = CreateRaycast(m_EverythingMask);

            // Default end
            Vector3 endPosition = m_CurrentOrigin.position + (m_CurrentOrigin.forward * m_Distance);

            if (hit.collider != null)
                endPosition = hit.point;

            //if (!gameManager.gameStarted)
            //{
            //    m_LineRenderer.enabled = true;
            //    // Check hit //REMOVED
            //    if (hit.collider != null && !hit.collider.CompareTag("Bullet"))
            //        endPosition = hit.point;
            //}
            //else
            //    m_LineRenderer.enabled = false;

            // Set position
            m_LineRenderer.SetPosition(0, m_CurrentOrigin.position);
            m_LineRenderer.SetPosition(1, endPosition);

            return endPosition;
        }

        private void UpdateOrigin(OVRInput.Controller controller, GameObject controllerObject)
        {
            // Set origin of pointer
            m_CurrentOrigin = controllerObject.transform;

            Debug.Log("m_CurrentOrigin: " + m_CurrentOrigin);
            m_LineRenderer.enabled = true;

            // Is the laser visible? // REMOVED
            //if (controller == OVRInput.Controller.Touchpad)
            //    m_LineRenderer.enabled = false;
            //else
            //    m_LineRenderer.enabled = true;
        }

        private GameObject UpdatePointerStatus()
        {
            // Create ray
            RaycastHit hit = CreateRaycast(m_InteractableMask);

            // Check hit
            if (hit.collider != null)
                return hit.collider.gameObject;

            // Return
            return null;
        }

        private RaycastHit CreateRaycast(int layer)
        {
            RaycastHit hit;
            if (m_CurrentOrigin == null)
                return hit = new RaycastHit();

            Ray ray = new Ray(m_CurrentOrigin.position, m_CurrentOrigin.forward);
            Physics.Raycast(ray, out hit, m_Distance, layer);

            return hit;
        }

        private void SetLineColor()
        {
            if (!m_LineRenderer)
                return;

            Color endColor = Color.white;
            endColor.a = 0.0f;

            m_LineRenderer.endColor = endColor;
        }

        private void ProcessTouchPadDown()
        {
            if (!m_CurrentObject)
                return;

            Interactable interactable = m_CurrentObject.GetComponent<Interactable>();
            interactable.Pressed();
        }
    }
}
