﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace VReddo.ShooterGame
{
    public class PlayerEvents : ITickable, IInitializable, ILateDisposable
    {
        //Events
        public Action OnTouchPadUp = null;
        public Action OnTouchPadDown = null;
        public Action OnIndexTriggerDown = null;
        public Action OnIndexTriggerUp = null;
        public Action OnSetController = null;
        public Action <OVRInput.Controller, GameObject> OnControllerSource = null;

        // Anchors
        public GameObject m_LeftAnchor;
        public GameObject m_RightAnchor;
        public GameObject m_HeadAnchor;

        // Input
        private Dictionary<OVRInput.Controller, GameObject> m_ControllerSets = null;
        private OVRInput.Controller m_InputSource = OVRInput.Controller.None;
        private OVRInput.Controller m_Controller = OVRInput.Controller.None;
        private bool m_InputActive = true;
        private GameManager gameManager;

        #region Zenject Methods
        [Inject]
        public void Construct(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        public void Initialize()
        {
            OVRManager.HMDMounted += PlayerFound;
            OVRManager.HMDUnmounted += PlayerLost;

            m_ControllerSets = CreateControllerSets();
        }

        public void Tick()
        {
            // Check for active input
            if (!m_InputActive)
                return;

            // Checking if a controller exist
            CheckForController();

            // Check for input source
            CheckInputSource();

            // Check for actual input
            Input();
        }

        public void LateDispose()
        {
            OVRManager.HMDMounted -= PlayerFound;
            OVRManager.HMDUnmounted -= PlayerLost;
        }
        #endregion

        #region Public Methods
        private void CheckForController()
        {
            OVRInput.Controller controllerCheck = m_Controller;

            // Right Remote (Android)
            if (OVRInput.IsControllerConnected(OVRInput.Controller.RTrackedRemote))
            {
                controllerCheck = OVRInput.Controller.RTrackedRemote;
            }

            // Left Remote (Android)
            if (OVRInput.IsControllerConnected(OVRInput.Controller.LTrackedRemote))
            {
                controllerCheck = OVRInput.Controller.LTrackedRemote;
            }

            // If no controllers, headset
            if (OVRInput.IsControllerConnected(OVRInput.Controller.RTrackedRemote) &&
                OVRInput.IsControllerConnected(OVRInput.Controller.LTrackedRemote))
            {
                controllerCheck = OVRInput.Controller.Touchpad;
            }

            // Right Controller(Quest and Rift)
            if (OVRInput.IsControllerConnected(OVRInput.Controller.RTouch))
            {
                controllerCheck = OVRInput.Controller.RTouch;
            }

            // Update
            m_Controller = UpdateSource(controllerCheck, m_Controller);
        }

        private void CheckInputSource()
        {
            if (OVRInput.GetActiveController() == OVRInput.Controller.Touch ||
                OVRInput.GetActiveController() == OVRInput.Controller.LTouch)
                return;

            // Update
            m_InputSource = UpdateSource(OVRInput.GetActiveController(), m_InputSource);
        }

        private void Input()
        {
            if (OVRInput.GetActiveController() == OVRInput.Controller.RTrackedRemote ||
                OVRInput.GetActiveController() == OVRInput.Controller.LTrackedRemote)
            {
                // Touchpad Down
                if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad))
                {
                    if (OnTouchPadDown != null)
                        OnTouchPadDown();
                }

                // Touchpad Up
                if (OVRInput.GetUp(OVRInput.Button.PrimaryTouchpad))
                {
                    if (OnTouchPadUp != null)
                        OnTouchPadUp();
                }

                // Index Trigger Down
                if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
                {
                    if (OnIndexTriggerDown != null)
                        OnIndexTriggerDown();
                }

                // Index Trigger Up
                if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger))
                {
                    if (OnIndexTriggerUp != null)
                        OnIndexTriggerUp();
                }
            }
            else
            {
                // Index Trigger Down
                if (OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger))
                {
                    if (OnIndexTriggerDown != null)
                        OnIndexTriggerDown();
                }

                // Index Trigger Up
                if (OVRInput.GetUp(OVRInput.RawButton.RIndexTrigger))
                {
                    if (OnIndexTriggerUp != null)
                        OnIndexTriggerUp();
                }

                // A Button Press
                if (OVRInput.GetDown(OVRInput.RawButton.A))
                {
                    if (OnTouchPadDown != null)
                        OnTouchPadDown();
                }
            }
        }

        private OVRInput.Controller UpdateSource(OVRInput.Controller check, OVRInput.Controller previous)
        {
            // If values are the same, return
            if (check == previous)
                return previous;

            // Get controller object
            GameObject controllerObject = null;
            m_ControllerSets.TryGetValue(check, out controllerObject);

            //Debug.Log("---------------------controllerObject: " + controllerObject);
            // If no controller, set to the head
            if (controllerObject == null)
                controllerObject = gameManager.m_HeadAnchor;

            // Send out event
            if (OnControllerSource != null)
                OnControllerSource(check, controllerObject);

            // Return new value
            return check;
        }

        private void PlayerFound()
        {
            m_InputActive = true;
        }

        private void PlayerLost()
        {
            m_InputActive = false;
        }

        private Dictionary<OVRInput.Controller, GameObject> CreateControllerSets()
        {
            Dictionary<OVRInput.Controller, GameObject> newSets = new Dictionary<OVRInput.Controller, GameObject>()
            {
                { OVRInput.Controller.LTrackedRemote, gameManager.m_LeftAnchor },
                { OVRInput.Controller.RTrackedRemote, gameManager.m_RightAnchor },
                { OVRInput.Controller.RTouch, gameManager.m_RightAnchor },
                //{ OVRInput.Controller.Touchpad, gameManager.m_HeadAnchor }
            };

            return newSets;
        }
        #endregion
    }
}
