﻿using UnityEngine;

namespace VReddo.ShooterGame
{
    public class Interactable : MonoBehaviour
    {
        public void Pressed()
        {
            MeshRenderer renderer = GetComponent<MeshRenderer>();
            bool flip = !renderer.enabled;

            renderer.enabled = flip;
        }
    }
}
