How to Play:
- Shoot red targets to score points. Points given varies depending on target position.
- Avoid hitting the green targets. Hitting one will give you a deduction of 2 points.
- Each player is given 60 seconds to shoot as many red targets as possible.
- Accuracy will play a big factor on the final score. It will serve as multiplier at the end of the game.

Getting Started:
Run on Unity Editor
- Enable Player gameobject on the hierarchy.
- EventSystem gameobject on the hierachy should have OVR Input Module deactivated and Standalone Input Module activated.
- Canvas gameobject on the hierachy should have OVR Raycaster deactivated and Graphic Raycaster activated.

Run on VR
- Make sure Player gameobject is disabled.
- EventSystem gameobject on the hierachy should have OVR Input Module activated and Standalone Input Module deactivated.
- Canvas gameobject on the hierachy should have OVR Raycaster activated and Graphic Raycaster deactivated.
- Switch to android platform on the Build Settings.
- On the PlayerSettings->XR Setting, make sure that virtual reality is marked check and Oculus is added to VR SDK.
- On the PlayerSettings->Other Settings, Set Minimum API Level to API Level 21 'Lollipop'.

Built with:
Unity 2018.3.5.f1

Other Tools used:
Zenject
Oculus